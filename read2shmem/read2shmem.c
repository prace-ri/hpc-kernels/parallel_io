#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#define MAX_FILE_SIZE_GB 4
#define BUFLEN 1024

int world_rank, world_size;
char filename[BUFLEN];

void print_info_world();
void print_info_intranode();
void print_info_internode();
void print_info_read(const MPI_Status *status, double time);

void broadcast_filename(int argc, char *argv[]);

void do_something(int *arr, size_t n);

int main(int argc, char *argv[])
{
   int master;
   int node_master;
   int key, color;
   int disp_unit, rank;
   int verbose = 1;
   MPI_Comm intranode_comm, internode_comm;
   int intranode_rank;
   MPI_File fh;
   MPI_Offset file_size, file_size_in_gb;
   MPI_Aint local_window_size, window_size;
   MPI_Win memory_window;
   int *local_base_ptr, *base_ptr;
   size_t n_elements;

   // Initialize MPI
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
   MPI_Comm_size(MPI_COMM_WORLD, &world_size);
   master = world_rank == 0;

   // We want the program to stop on I/O errors
   // -> Change the default I/O error hander from MPI_ERRORS_RETURN to MPI_ERRORS_ARE_FATAL
   // Notes:
   //  * An individual I/O error handler can be associated to each file handle.
   //  * The default I/O error handler is associated to the null file handle, i.e. MPI_FILE_NULL.
   MPI_File_set_errhandler(MPI_FILE_NULL, MPI_ERRORS_ARE_FATAL);

   if(verbose) print_info_world();

   // Broadcast name of file to open
   if(master) printf("\nBroadcasting filename...\n");
   broadcast_filename(argc, argv);

   if(master) printf("\nCreating MPI communicators...\n");
   // Create sub-communicators of MPI_COMM_WORLD containing processes sharing one address space (i.e. living on one node)
   key = 0;
   MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, key, MPI_INFO_NULL, &intranode_comm);
   if(verbose) print_info_intranode(intranode_comm);

   // Create sub-communicator of "node-masters" (communicator containing exactly one process from each node)
   MPI_Comm_rank(intranode_comm, &intranode_rank);
   node_master = intranode_rank == 0;
   color = node_master ? 1 : MPI_UNDEFINED;
   MPI_Comm_split(MPI_COMM_WORLD, color, key, &internode_comm);
   if(verbose) print_info_internode(internode_comm);
   // Note: internode_comm is MPI_COMM_NULL for all "non-node-masters", i.e. for all processes who supplied MPI_UNDEFINED as color

   if(node_master) {
      // Collective file open by "node-masters"
      if(master) printf("\nOpening file %s...\n", filename);
      MPI_File_open(internode_comm, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      MPI_File_get_size(fh, &file_size);

      file_size_in_gb = file_size >> 30;
      if(file_size_in_gb > MAX_FILE_SIZE_GB) {
         fprintf(stderr, "File is too large (max file size: %d GB)\n", MAX_FILE_SIZE_GB);
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
      local_window_size = file_size;
   } else {
      local_window_size = 0;
   }

   // Create shared memory region for file contents
   if(master) printf("\nCreating shared memory region (%d MB)...\n", file_size >> 20);
   disp_unit = sizeof(int);
   MPI_Win_allocate_shared(local_window_size, disp_unit, MPI_INFO_NULL, intranode_comm, &local_base_ptr, &memory_window);

   // Get pointer to start of shared memory region
   rank = 0;
   MPI_Win_shared_query(memory_window, rank, &window_size, &disp_unit, &base_ptr);
   n_elements = window_size / sizeof(int);

   // Read file contents to shared memory region (only "node-masters" are involved)
   if(master) printf("\nReading file...\n");
   if(node_master) {
      MPI_Status status;
      double time = -MPI_Wtime();
      MPI_File_read_all(fh, base_ptr, n_elements, MPI_INT, &status);
      time += MPI_Wtime();
      print_info_read(&status, time);

      // Close file
      MPI_File_close(&fh);
   }

   // End write access epoch and start read access epoch for shared memory window
   MPI_Win_fence(0, memory_window);

   // Do something with the data read
   do_something(base_ptr, n_elements);

   // Cleanup
   if(master) printf("\nCleaning up...\n");
   MPI_Win_free(&memory_window);
   if(node_master) MPI_Comm_free(&internode_comm);
   MPI_Comm_free(&intranode_comm);

   if(master) printf("\nDone.\n");
   MPI_Finalize();
   return EXIT_SUCCESS;
}

void broadcast_filename(int argc, char *argv[])
{
   int len;

   if(world_rank == 0) {
      if(argc < 2) {
         fprintf(stderr,
           "Error: No input file specified\n"
           "\nUsage: [mpi-launcher-and-args] %s [filename]\n", argv[0]);
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
      len = strlen(argv[1])+1; // include terminating null byte
      if(len > BUFLEN) {
         fprintf(stderr, "Error: File name too long (maximum length is %d).\n", BUFLEN-1);
         MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      }
      memcpy(filename, argv[1], len);
   }

   MPI_Bcast(filename, BUFLEN, MPI_CHAR, 0, MPI_COMM_WORLD);
}

void print_info_world()
{
   if(world_rank == 0) {
      printf("\n%03d: Size of MPI_COMM_WORLD: %d\n", world_rank, world_size);
   }
}

void print_info_intranode(MPI_Comm intranode_comm)
{
   int intranode_rank, intranode_size;

   MPI_Comm_rank(intranode_comm, &intranode_rank);
   MPI_Comm_size(intranode_comm, &intranode_size);
   if(intranode_rank == 0) {
      printf("\n%03d: Size of intranode_comm: %d\n", world_rank, intranode_size);
   }
}

void print_info_internode(MPI_Comm internode_comm)
{
   int internode_rank, internode_size;

   if(internode_comm != MPI_COMM_NULL) {
      MPI_Comm_rank(internode_comm, &internode_rank);
      MPI_Comm_size(internode_comm, &internode_size);

      if(internode_rank == 0) {
         printf("\n%03d: Size of internode_comm: %d\n", world_rank, internode_size);
      }
   }
}

void print_info_read(const MPI_Status *status, double time)
{
   int count;
   double mbs;

   MPI_Get_count(status, MPI_INT, &count);
   mbs = count * sizeof(int) / time / 1024. / 1024.;

   printf("%03d: Read %d integers in %f seconds (%f MB/s)\n", world_rank, count, time, mbs);
}

void do_something(int *arr, size_t n) {
   size_t i;
   int s = 0;

   for(i = 0; i < n; i++) {
      s ^= arr[i];
   }

   printf("%03d: Checksum: %d\n", world_rank, s);
}

